
//Creating my first classs in ES6

class Person{
  constructor(name = 'Anonymous', age = 0){
    this.name = name;
    this.age = age;
  }

  getGreeting(){
    return `Hi. I'm ${this.name}`;
  }

  getDescritpion(){
  return `${this.name} is ${this.age} years old.`
  }
}

const me = new Person("Pablo",26);
console.log(me.getGreeting());
console.log(me.getDescritpion());
