const fullName = 'Pablo Almendras';

const getFirstName = (x) => {

  return x.split(' ')[0];
};

const getFirstNameArrow = (x) => x.split(' ')[0];


console.log(getFirstName(getFirstName(fullName)));
console.log(getFirstName(getFirstNameArrow(fullName)));
