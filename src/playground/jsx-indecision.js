class IndecisionApp extends React.Component{
  render(){

    const title = 'Indecision App';
    const subTitle = 'Your life in hands of computers';
    const options = ['Thing 1', 'THing 2', 'Thing 3'];
    return (
      <div>
      <Header title={title} subtitle={subTitle} />
      <Action />
      <Options options={options} />
      <AddOption />
      </div>
    );
  }
}

class Header extends React.Component {
  render(){
    return (
      <div>
      <h1>{this.props.title}</h1>
      <h2>{this.props.subtitle}</h2>
      </div>
    );
  }
}

class Action extends React.Component{
  handlePick() {
    alert('Hola!');
  }

  render(){
    return (
      <div>
          <button onClick={this.handlePick}>What should I do?</button>
      </div>
    );
  }
}

class Option extends React.Component{
  render(){
    return (
      <div>
      {this.props.optionText}
      </div>
    );
      }
}


class Options extends React.Component{
  constructor(props){
    super(props);
    this.onRemoveAll = this.onRemoveAll.bind(this);
  }
  onRemoveAll(){
    console.log(this.props.options);
  }
  render(){
    return (
      <div>

      <h3>You have {this.props.options.length} options to choose</h3>

      <button onClick={this.onRemoveAll}>Remove all</button>

      {this.props.options.map((option) => <Option key={option} optionText={option} />)}

      </div>
    );
  }
}
class AddOption extends React.Component{
  onFormSubmit(e) {
    e.preventDefault();

    const option = e.target.elements.option.value.trim();

    if (option.length > 0) {
      alert(option);
      e.target.elements.option.value='';

    }
  };
  render(){
    return (
      <div>
        <h3>Add a new option</h3>
        <form onSubmit={this.onFormSubmit}>
          <input type="text" name="option"/>
          <button>Add Option</button>
        </form>
      </div>
    );
  }
}


ReactDOM.render(<IndecisionApp />, document.getElementById("app"))
