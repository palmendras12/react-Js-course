
//Creating my first classs in ES6

class Person{
  constructor(name = 'Anonymous', age = 0){
    this.name = name;
    this.age = age;
  }

  getGreeting(){
    return `Hi. I'm ${this.name}`;
  }

  getDescritpion(){
  return `${this.name} is ${this.age} years old.`
  }
}

class Student extends Person{
  constructor(name, age, major){
    super(name, age)
    this.major = major;
  }

  hasMajor(){
    return !!this.major;
  }

  getDescritpion(){
    let description = super.getDescritpion();
    if (this.hasMajor()){
      description += ` Their Major is ${this.major}`
    }
    return description;
  }
}


class Traveler extends Person{
  constructor(name, age, homeLocation){
    super(name, age);
    this.homeLocation=homeLocation;
  }

  hasLocation(){
    return !!this.homeLocation;
  }

  getDescritpion(){
    let description = super.getDescritpion();

    if (this.hasLocation()){
      description+= `I'm visiting ${this.homeLocation}`;
    }

    return description;
  }

}

const me = new Traveler('Pablo Almendras', 26, 'Angol');
const other = new Traveler();

console.log(me.getDescritpion());
console.log(other.getDescritpion());
