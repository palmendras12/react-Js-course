console.log('App.js is running!');
const appRoot = document.getElementById('app');

// first I have to build my template. Y will Rerender this template with every change.

// as the previous example I will write a function to render the template

let showDetails = true;


const onShowToggle = () => {
    showDetails = !showDetails
    renderTemplate();
};


const renderTemplate = () =>{

    // Now I write my renderTemplate
    const template = (
      <div>
        <h1>Visibility Toggle</h1>

        <p><button onClick={onShowToggle}>{showDetails ? 'Ocultar detalles' : 'Mostrar detalles'}</button></p>

        {showDetails ? <div><p>'Here are your options'</p></div> : <div></div>}

      </div>
    );

    // I have to Render the renderTemplate
    console.log(showDetails);
    ReactDOM.render(template, appRoot);
};



// This function refresh the template
renderTemplate();
