'use strict';

console.log('App.js is running!');
var appRoot = document.getElementById('app');

// first I have to build my template. Y will Rerender this template with every change.

// as the previous example I will write a function to render the template

var showDetails = true;

var onShowToggle = function onShowToggle() {
  showDetails = !showDetails;
  renderTemplate();
};

var renderTemplate = function renderTemplate() {

  // Now I write my renderTemplate
  var template = React.createElement(
    'div',
    null,
    React.createElement(
      'h1',
      null,
      'Visibility Toggle'
    ),
    React.createElement(
      'p',
      null,
      React.createElement(
        'button',
        { onClick: onShowToggle },
        showDetails ? 'Ocultar detalles' : 'Mostrar detalles'
      )
    ),
    showDetails ? React.createElement(
      'div',
      null,
      React.createElement(
        'p',
        null,
        '\'Here are your options\''
      )
    ) : React.createElement('div', null)
  );

  // I have to Render the renderTemplate
  console.log(showDetails);
  ReactDOM.render(template, appRoot);
};

// This function refresh the template
renderTemplate();
